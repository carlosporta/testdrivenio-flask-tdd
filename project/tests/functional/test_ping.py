def test_ping(test_app):
    client = test_app.test_client()
    resp = client.get("/ping")
    json = resp.json
    assert resp.status_code == 200
    assert "pong" in json["message"]
    assert "success" in json["status"]
